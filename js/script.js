updateTheme();

function toggleTheme() {
    if (localStorage.getItem('style') == "dark") {
        localStorage.setItem('style', 'light');
    } else {
        localStorage.setItem('style', 'dark');
    }
    updateTheme();
}

function updateTheme() {
    var newStyleSheet=document.createElement('link');
    newStyleSheet.rel='stylesheet';
    if (localStorage.getItem('style') == "dark") {
        newStyleSheet.href='css/colors.css';
    } else {
        newStyleSheet.href='css/colors_alt.css';
    }
    document.getElementsByTagName("head")[0].appendChild(newStyleSheet);
}